# Create a library called "engine" which includes the source file "*.cxx".
# The extension is already found. Any number of sources could be listed here.


file(GLOB ENGINE_FILES *.cpp)
add_library (engine STATIC ${ENGINE_FILES})

# Includes only for this target.
target_include_directories (engine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories (engine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../strings)
target_include_directories (engine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../maths)
#target_include_directories (engine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../networking)
target_include_directories (engine PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../serialisation)

#Engine
target_link_libraries (engine LINK_PUBLIC strings)
target_link_libraries (engine LINK_PUBLIC maths)
#target_link_libraries (engine LINK_PUBLIC networking)
target_link_libraries (engine LINK_PUBLIC serialisation)
